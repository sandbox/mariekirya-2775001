Node Theme Picker
=================

Usage
-----

1. Install the module.
2. Rebuild Cache.
3. Navigate to any node and look under the advanced box for 'Theme Settings'.
4. Change the node theme to the one you wish. This can also be progromatically
   done by editing the `node_theme` field on the node entity.
