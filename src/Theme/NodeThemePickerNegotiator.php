<?php

/**
 * @file
 * Contains \Drupal\node_theme_picker\Theme\NodeThemePickerNegotiator.
 */

namespace Drupal\node_theme_picker\Theme;

use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Theme\ThemeNegotiatorInterface;

class NodeThemePickerNegotiator implements ThemeNegotiatorInterface {

  public function applies(RouteMatchInterface $route_match) {
    if ($route_match->getRouteName() != 'entity.node.canonical' ||
      !($node = $route_match->getParameter('node'))) {
      return FALSE;
    }

    if ($node->node_theme->value != NULL) {
      return TRUE;
    }

    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function determineActiveTheme(RouteMatchInterface $route_match) {
    // this->applies(...) has already checked if we are to use a theme, so we
    // just return the theme without checking.
    $node = $route_match->getParameter('node');
    return $node->node_theme->value;
  }
}
